import React from 'react'
import { inject, observer } from 'mobx-react'
import AuthStore from '../stores/auth'
import UsBuildStore from '../stores/usbuild'
import Input from './Input'

@inject('auth', 'usbuild')
@observer
export default class CreateTeam extends React.Component<{
  auth?: AuthStore
  usbuild?: UsBuildStore
  onCompleted?: () => void
}> {
  state = {
    nameText: '',
  }

  createTeam = async () => {
    const { usbuild, auth } = this.props
    try {
      await auth.send({
        to: usbuild.contractAddress,
        data: usbuild.contract.methods.createTeam(auth.activeAddress, this.state.nameText).encodeABI(),
      })
      this.setState({
        nameText: '',
      })
      this.props.onCompleted && this.props.onCompleted()
    } catch (err) {
      alert('there was a problem creating the team')
    }
  }

  render() {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
      }}>
        <div>
          Creating a new team
        </div>
        <Input
          placeholder="name"
          onChange={(e: any) => {
            this.setState({
              nameText: e.target.value,
            })
          }}
          value={this.state.nameText}
        />
        <button onClick={this.createTeam}>Create</button>
      </div>
    )
  }
}

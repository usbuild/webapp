import React from 'react'
import { white, offBlack } from '../Colors'
import UsBuildStore, { Team } from '../stores/usbuild'
import AuthStore from '../stores/auth'
import { inject, observer } from 'mobx-react'
import uuid from 'uuid'
import UserCell from './UserCell'
import Link from './Link'

@inject('auth', 'usbuild')
@observer
export default class TeamCell extends React.Component<{
  team: Team
  auth?: AuthStore
  usbuild?: UsBuildStore
  key?: any
}> {
  render() {
    const { team, key, auth, usbuild } = this.props
    if (team.isLoading) return <>Loading...</>
    return (
      <div
        key={key}
        style={{
          display: 'flex',
          flexDirection: 'column',
          backgroundColor: offBlack,
          padding: 8,
          alignSelf: 'center',
          margin: 8,
        }}
      >
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex' }}>
            <div>Team:</div>
            <div style={{ width: 8 }} />
            <Link to={`/team/${team.contractAddress}`}>
              {team.name}
            </Link>
          </div>
          <div>
            {team.users.length} Members
          </div>
        </div>
        <div style={{ height: 10 }} />
        <div style={{
          display: 'flex',
          flexDirection: 'column',
        }}>
          {team.users.map((user, index) => (
            <UserCell reverse={!!(index % 2)} key={index} user={user} />
          ))}
        </div>
        <div>
          Address for payment: <a
          href={`https://rinkeby.etherscan.io/address/${team.contractAddress}`}
          target="_blank"
          style={{ color: white }}
          >
          {team.contractAddress}
          </a>
        </div>
      </div>
    )
  }
}

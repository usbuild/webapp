import React from 'react'
import { Redirect } from 'react-router-dom'

export default class Link extends React.Component<{
  to: string
  style?: any
}> {
  state = {
    showingRedirect: false,
  }
  render() {
    const { to } = this.props
    if (this.state.showingRedirect &&
      window.location.pathname !== to) return <Redirect push to={to} />
    return (
      <div
        style={{
          cursor: 'pointer',
          textDecoration: 'underline',
          ...(this.props.style || {}),
        }}
        onClick={() => {
          this.setState({ showingRedirect: true })
        }}
      >
        {this.props.children}
      </div>
    )
  }
}

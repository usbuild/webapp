import React from 'react'
import { white, black } from '../Colors'

export default (props: {
  visible: boolean
  children?: any
  onClick?: () => void
}) => (
  <>
    {props.visible ? (
      <div
        style={{
          position: 'fixed',
          top: 0,
          right: 0,
          left: 0,
          bottom: 0,
          display: 'flex',
          backgroundColor: 'rgba(0, 0, 0, 0.4)',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onClick={props.onClick || (() => {})}
      >
        <div
          style={{
            backgroundColor: white,
            boxShadow: `0px 0px 8px ${white}55`,
            padding: 8,
            color: black,
            maxWidth: 500,
            maxHeight: 800,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            position: 'sticky',
            zIndex: 10,
          }}
          onClick={(e: any) => e.stopPropagation()}
        >
          {props.children}
        </div>
      </div>
    ) : null}
  </>
)

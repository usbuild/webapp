import React from 'react'
import Popup from './Popup'
import SignupView from './SignupView'
import { inject, observer } from 'mobx-react'
import UsBuildStore from '../stores/usbuild'
import AuthStore from '../stores/auth'
import Link from './Link'
import DisplayStore from '../stores/display'
import { goldLight } from '../Colors'

@inject('auth', 'usbuild', 'display')
@observer
export default class Header extends React.Component<{
  usbuild?: UsBuildStore
  auth?: AuthStore
  title?: string
  display?: DisplayStore
}> {
  state = {
    showingSignup: false,
    activeAddress: '0',
  }

  async componentDidMount() {
    await this.props.usbuild.loadUser(this.props.auth.activeAddress)
  }

  render() {
    const { usbuild, auth, title, display } = this.props
    const user = usbuild.user(auth.activeAddress)
    return (
      <>
        <Popup
          onClick={() => {
            this.setState({
              showingSignup: false,
            })
          }}
          visible={this.state.showingSignup}
        >
          <SignupView />
        </Popup>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          margin: 8,
          maxWidth: 900,
          minWidth: Math.min(700, display.width),
        }}>
          <div style={{
            fontSize: 30,
          }}>
            <Link style={{ textDecoration: 'none' }} to="/">
              {title || (
                <>
                  <span style={{ color: goldLight}}>us</span>Build
                </>
              )}
            </Link>
          </div>
          {user.contractAddress ? (
            <div>
            <Link to={`/user/${user.contractAddress}`}>
              {user.name}
            </Link>
            </div>
          ) : (
            <div onClick={() => {
              this.setState({
                showingSignup: true,
              })
            }}>
              Signup
            </div>
          )}
        </div>
      </>
    )
  }
}

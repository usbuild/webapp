import React from 'react'
import { GitlabLogo } from './Icons'
import { inject, observer } from 'mobx-react'
import UsBuildStore from '../stores/usbuild'
import { white } from '../Colors'

@inject('usbuild')
export default class Footer extends React.Component<{
  usbuild?: UsBuildStore
}> {
  render() {
    const { usbuild } = this.props
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flex: 1
      }}
      >
        <div style={{ minHeight: 50, flex: 1 }} />
        <div>
          <a
            href={`https://rinkeby.etherscan.io/address/${usbuild.contractAddress}`}
            target="_blank"
            style={{
              textDecoration: 'none',
              color: white,
            }}
          >
            {usbuild.contractAddress}
          </a>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <a href='https://gitlab.com/usBuild' target="_blank">
            <GitlabLogo />
          </a>
        </div>
      </div>
    )
  }
}

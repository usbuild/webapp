import React from 'react'

export default class Input extends React.Component<{
  onChange?: (e: any) => void
}> {
  render() {
    return (
      <input
        type="text"
        {...this.props}
      />
    )
  }
}

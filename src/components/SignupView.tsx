import React from 'react'
import { white, black } from '../Colors'
import { inject, observer } from 'mobx-react'
import AuthStore from '../stores/auth'
import UsBuildStore from '../stores/usbuild'

@inject('auth', 'usbuild')
@observer
export default class SignupView extends React.Component<{
  auth?: AuthStore
  usbuild?: UsBuildStore
}> {
  signup = async () => {
    const { auth, usbuild } = this.props
    if (!window.ethereum) {
      return window.open('https://metamask.io/', '_blank')
    }
    try {
      await auth.send({
        to: usbuild.contractAddress,
        data: usbuild.contract.methods.createUser(auth.activeAddress).encodeABI(),
      })
      alert('Transaction Completed')
    } catch (err) {
      alert('Transaction Error')
    }
  }

  render() {
    return (
      <>
        <img
          src={require('../../static/images/sky_background.jpg')}
          style={{
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width: '100%',
            height: '100%',
            position: 'absolute',
            zIndex: -2,
            objectFit: 'cover',
          }}
        />
        <div
          style={{
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width: '100%',
            height: '100%',
            position: 'absolute',
            zIndex: -1,
            backgroundColor: `${white}aa`,
          }}
        />
        <div
          style={{
            fontSize: 23,
            padding: 8,
          }}
        >
          Join usBuild
        </div>
        <div style={{ height: 20 }} />
        <div
          style={{
            textAlign: 'center',
            maxWidth: 250,
          }}
        >
          Register with your Ethereum address to make or join a team and begin receiving funds.
        </div>
        <div style={{ height: 20 }} />
        <div
          style={{
            borderRadius: 4,
            padding: 8,
            boxShadow: `0px 0px 4px ${black}33`,
            margin: 8,
            cursor: 'pointer',
          }}
          onClick={this.signup}
        >
          Join With MetaMask
        </div>
      </>
    )
  }
}

import React from 'react'
import UsBuildStore, { User } from '../stores/usbuild'
import { inject, observer } from 'mobx-react'
import AuthStore from '../stores/auth'
import Link from './Link'
import Img from 'react-image'

@inject('auth', 'usbuild')
@observer
export default class UserCell extends React.Component<{
  user: User
  auth?: AuthStore
  usbuild?: UsBuildStore
  key?: any
  reverse?: boolean
}> {
  state = {
    nameText: '',
  }

  sampleWorkRef = React.createRef<div>()

  componentDidMount() {
    if (this.sampleWorkRef.current) {
      this.sampleWorkRef.current.addEventListener('wheel', this.handleWheel)
    }
  }

  componentDidUpdate() {
    if (this.sampleWorkRef.current) {
      this.sampleWorkRef.current.addEventListener('wheel', this.handleWheel)
    }
  }

  componentWillUnmount() {
    if (this.sampleWorkRef.current) {
      this.sampleWorkRef.current.removeEventListener('wheel', this.handleWheel)
    }
  }

  handleWheel = (e: any) => {
    e.preventDefault()
    const element = this.sampleWorkRef.current
    if (!element) return
    this.sampleWorkRef.current.scrollTo(this.sampleWorkRef.current.scrollLeft + e.deltaY, 0)
  }

  renderSampleWork = () => (
    <div>
      Sample Work
      <div style={{ height: 10 }} />
      <div
        ref={this.sampleWorkRef}
        style={{
          display: 'flex',
          flexWrap: 'nowrap',
          overflowX: 'auto',
          maxWidth: 500,
          touchAction: 'pan-x',
        }}
      >
      {this.props.user.sampleUrls.map((url, index) => (
        <div key={index} style={{ margin: 4 }}>
          <a href={url} target="_blank">
            <Img
              src={`https://backend.usbuild.now.sh/screenshot?url=${url}&width=250&height=250`}
              style={{
                maxWidth: 250,
                maxHeight: 250,
              }}
            />
          </a>
        </div>
        ))}
      </div>
    </div>
  )
  renderSkills = () => (
    <div style={{ display: 'flex', flexDirection: 'column'}}>
      <div style={{ minWidth: 100 }}>
        Skills
      </div>
      <div style={{ height: 10 }} />
      {this.props.user.skills.map((skill, index) => (
        <div key={index}>
          {skill}
        </div>
      ))}
    </div>
  )
  render() {
    const { reverse, key, user, usbuild, auth } = this.props
    if (user.isLoading) return <>Loading...</>
    return (
      <div
        key={key}
        style={{
          display: 'flex',
          flexDirection: 'column',
          margin: 8,
        }}
      >
        <div style={{ display: 'flex', justifyContent: reverse ? 'flex-end' : 'flex-start' }}>
          <Link to={`/user/${user.contractAddress}`}>
            {user.name || user.contractAddress}
          </Link>
          <div style={{ width: 8 }} />
          <div>
            {user.email ? ` - ${user.email}` : ''}
          </div>
        </div>
        <div style={{ height: 10 }} />
        <div style={{
          display: 'flex',
          justifyContent: 'space-between',
        }}>
          {reverse ? this.renderSampleWork() : this.renderSkills()}
          <div style={{ width: 20 }} />
          {reverse ? this.renderSkills() : this.renderSampleWork()}
        </div>
      </div>
    )
  }
}

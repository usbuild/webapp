import Web3 from 'web3'

declare global {
  interface Window {
    ethereum: any
  }
  namespace NodeJS {
    interface Global {
      web3: Web3
    }
  }
  let web3: Web3
}

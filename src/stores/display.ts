import { observable } from 'mobx'

const HEIGHT_OFFSET = 40

export default class DisplayStore {
  @observable height: number = window.innerHeight - HEIGHT_OFFSET
  @observable width: number = window.innerWidth

  constructor() {
    window.addEventListener('resize', () => {
      this.height = self.innerHeight - HEIGHT_OFFSET
      this.width = self.innerWidth
    })
  }
}

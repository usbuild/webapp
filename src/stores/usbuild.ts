import { observable } from 'mobx'
import Web3 from 'web3'
import USBUILD_ABI from './usbuildABI'
import USER_ABI from './userABI'
import TEAM_ABI from './teamABI'
import idx from 'idx'
import ParallelPromise from '../ParallelPromise'
import BN from 'bn.js'

export default class UsBuildStore {
  @observable teams: Team[] = []
  @observable users: { [key: string]: User } = {}

  @observable contractAddress: string = '0xcf7D2e495a65C967BbBd1B10233809f58A1aA5B8'
  web3: Web3

  contract: any

  constructor() {
    const provider = new Web3.providers.WebsocketProvider('wss://rinkeby.infura.io/ws/v3/b1a4ca90c9964c329c07874b37e05436')
    // const provider = new Web3.providers.WebsocketProvider('ws://165.22.231.7:8547')
    this.web3 = new Web3(provider)
    this.contract = new this.web3.eth.Contract(USBUILD_ABI as any, this.contractAddress)
    this.contract.events.TeamCreated({}, (err: any, event: any) => {
      setTimeout(() => this.loadTeams(), 10000)
    })
    this.contract.events.UserCreated({}, (err: any, event: any) => {
      console.log(event)
      this.loadUser(event.user)
    })
    setTimeout(() => {
      Promise.all([
        this.loadTeams(),
        this.loadUser(idx(window, (_) => _.ethereum.selectedAddress)),
      ])
        .then(() => console.log('Finished first load'))
        .catch((err: any) => console.log('Error in first load', err))
    }, 500)
  }

  async loadTeams() {
    const teamCount = +await this.contract.methods.teamCount().call()
    this.teams = await ParallelPromise(teamCount, async (i) => {
      const team = await this.contract.methods.teams(i).call()
      return new Team(team, this.web3)
    })
  }

  user(address: string = '') {
    return this.users[address.toLowerCase()] || {}
  }

  async loadUser(address: string) {
    const code = await this.web3.eth.getCode(address)
    let _address = address
    if (code === '0x') {
      _address = await this.contract.methods.userAccounts(address).call()
      if (parseInt(_address) === 0) return
    }
    const user = new User(_address.toLowerCase(), this.web3)
    await user.initialLoad
    this.users[_address.toLowerCase()] = user
    this.users[address.toLowerCase()] = user
    return user
  }
}

export class ContractStore {
  web3: Web3
  contractAddress: string
  contract: any

  @observable isLoading = true
  initialLoad: Promise<any>

  @observable balance: BN|string = '0'

  constructor(address: string, abi: any, web3: Web3) {
    this.web3 = web3
    this.contractAddress = address
    this.contract = new this.web3.eth.Contract(abi, this.contractAddress)
    this.initialLoad = this.load()
  }

  async load() {
    this.loadBalance()
  }

  async loadBalance() {
    this.balance = await this.web3.eth.getBalance(this.contractAddress)
  }
}

export class Team extends ContractStore {
  @observable name: string = 'Team No Name'
  @observable users: User[] = []
  @observable inviteHashes: string[] = []
  @observable owner: string = ''

  constructor(address: string, web3: Web3) {
    super(address, TEAM_ABI, web3)
    this.contract.events.TeamUpdated({
      team: this.contractAddress,
    }, async (err: any, event: any) => {
      await this.load()
    })
  }

  async load() {
    await Promise.all([
      super.load(),
      this.loadUsers(),
      this.loadOwner(),
      this.loadName(),
      this.loadInvites(),
    ])
    this.isLoading = false
  }

  async loadOwner() {
    this.owner = (await this.contract.methods.owner().call()).toLowerCase()
  }

  async loadName() {
    this.name = (await this.contract.methods.name().call()) || 'Team No Name'
  }

  async loadUsers() {
    const userCount = +await this.contract.methods.userCount().call()
    this.users = await ParallelPromise(userCount, async (i) => {
      const user = await this.contract.methods.users(i).call()
      return new User(user, this.web3)
    })
  }

  async loadInvites() {
    const inviteCount = +await this.contract.methods.inviteHashCount().call()
    this.inviteHashes = await ParallelPromise(inviteCount, async (i) => {
      return await this.contract.methods.inviteHashes(i).call()
    })
  }

  async isInviteHashValid(hash: string) {
    const bytes = this.web3.utils.hexToBytes(hash)
    return await this.contract.methods.isInviteHashValid(bytes).call()
  }
}

export class User extends ContractStore {
  @observable name: string = 'User No Name'
  @observable email: string = ''
  @observable skills: string[] = []
  @observable sampleUrls: string[] = []
  @observable owner: string = ''

  constructor(address: string, web3: Web3) {
    super(address, USER_ABI, web3)
    this.contract.events.UserUpdated({
      user: idx(window, (_) => _.ethereum.selectedAddress),
    }, async (err: any, event: any) => {
      this.load()
    })
  }

  async load() {
    await Promise.all([
      super.load(),
      this.loadName(),
      this.loadEmail(),
      this.loadSkills(),
      this.loadOwner(),
      this.loadSamples(),
    ])
    this.isLoading = false
  }

  async loadOwner() {
    this.owner = (await this.contract.methods.owner().call()).toLowerCase()
  }

  async loadName() {
    this.name = (await this.contract.methods.name().call()) || 'User No Name'
  }

  async loadEmail() {
    this.email = await this.contract.methods.email().call()
  }

  async loadSkills() {
    const skillCount = +await this.contract.methods.skillCount().call()
    this.skills = await ParallelPromise(skillCount, async (i) => {
      return await this.contract.methods.skills(i).call()
    })
  }

  async loadSamples() {
    const sampleCount = +await this.contract.methods.sampleUrlCount().call()
    this.sampleUrls = await ParallelPromise(sampleCount, async (i) => {
      return await this.contract.methods.sampleUrls(i).call()
    })
  }
}

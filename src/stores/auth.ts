import { observable, computed } from 'mobx'
import BN from 'bn.js'
import idx from 'idx'
import debounce from 'lodash.debounce'

export default class AuthStore {

  constructor() {
    setTimeout(() => {
      if (!window.ethereum) return
      window.ethereum.on('accountsChanged', debounce((accounts: any[]) => {
        window.location.reload()
      }))
    }, 2000)
  }

  get activeAddress() {
    return idx(window, (_) => _.ethereum.selectedAddress.toLowerCase())
  }

  ethereumEnabled = false
  async enableEthereum() {
    if (this.ethereumEnabled) return
    if (window.ethereum) {
      console.log('Requesting MetaMask permission')
      await window.ethereum.enable()
      this.ethereumEnabled = true
    }
  }

  async send(params: {
    to: string,
    from?: string,
    data: any,
    value?: BN | string
  }) {
    await this.enableEthereum()
    await new Promise((rs, rj) => {
      window.ethereum.sendAsync({
        method: 'eth_sendTransaction',
        params: [{
          to: params.to,
          from: params.from || window.ethereum.selectedAddress,
          data: params.data,
          value: params.value,
        }]
      }, (err: any) => {
        if (err) return rj(err)
        rs()
      })
    })
  }
}

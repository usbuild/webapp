import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './Home'
import User from './User'
import { black, white } from './Colors'
import AuthStore from './stores/auth'
import UsBuildStore from './stores/usbuild'
import TeamInvite from './TeamInvite'
import Team from './Team'
import DisplayStore from './stores/display'

const stores = {
  auth: new AuthStore(),
  usbuild: new UsBuildStore(),
  display: new DisplayStore(),
}

document.body.style.backgroundColor = black
document.body.style.color = white

const appDiv = document.getElementById('app')
appDiv.style.fontFamily = 'Helvetica'
appDiv.style.display = 'flex'
appDiv.style.flexDirection = 'column'
appDiv.style.alignItems = 'center'

ReactDOM.render(
  <Provider {...stores}>
    <Router>
      <Route path="/" component={Home} exact />
      <Route path="/user/:address" component={User} />
      <Route path="/team/:address" component={Team} exact />
      <Route path="/team/invite/:address/:secret" component={TeamInvite} />
    </Router>
  </Provider>
, appDiv)

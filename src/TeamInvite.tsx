import React from 'react'
import Header from './components/Header'
import { white } from './Colors'
import { inject, observer } from 'mobx-react'
import AuthStore from './stores/auth'
import UsBuildStore, { Team } from './stores/usbuild'
import TeamCell from './components/TeamCell'
import Footer from './components/Footer'

@inject('auth', 'usbuild')
@observer
export default class TeamInvite extends React.Component<{
  match: {
    params: {
      address: string
      secret: string
    }
  }
  auth?: AuthStore
  usbuild?: UsBuildStore
}> {
  state = {
    team: undefined as Team,
    inviteValid: true,
  }

  async componentDidMount() {
    const { auth, usbuild } = this.props
    const { address, secret } = this.props.match.params
    const team = new Team(address, usbuild.web3)
    this.setState({ team })
    const hash = usbuild.web3.utils.keccak256(secret)
    const inviteValid = await team.isInviteHashValid(hash)
    console.log(inviteValid)
    this.setState({ inviteValid })
  }

  render() {
    const { secret } = this.props.match.params
    const { usbuild, auth } = this.props
    const { team, inviteValid } = this.state
    if (!team) return null
    return (
      <div style={{
        maxWidth: 900,
        margin: 'auto',
        padding: 8,
        color: white,
        display: 'flex',
        flexDirection: 'column',
      }}>
        <Header />
        <div style={{ height: 50 }} />
        {inviteValid ? (
          <>
            <div>Invite for team {team.name || 'Unnamed Team'}</div>
            <button onClick={async () => {
              try {
                await auth.send({
                  to: team.contractAddress,
                  data: team.contract.methods.joinTeam(Buffer.from(secret, 'utf8')).encodeABI(),
                })
              } catch (err) {
                console.log('Error joining team', err)
              }
            }}>Join Team</button>
          </>
        ) : (
          <div>This invite is invalid</div>
        )}
        <TeamCell team={team} />
        <Footer />
      </div>
    )
  }
}

import React from 'react'
import { white, black, offBlack } from './Colors'
import Header from './components/Header'
import { inject, observer } from 'mobx-react'
import UsBuildStore, { User } from './stores/usbuild'
import AuthStore from './stores/auth'
import Input from './components/Input'
import Footer from './components/Footer'
import DisplayStore from './stores/display'
import idx from 'idx'

@inject('auth', 'usbuild', 'display')
@observer
export default class _User extends React.Component<{
  match: {
    params: {
      address: string
    }
  }
  usbuild?: UsBuildStore
  auth?: AuthStore
  display?: DisplayStore
}> {
  state = {
    nameText: '',
    emailText: '',
    urlText: '',
    urlText2: '',
    skills: [] as string[],
    urls: [] as string[],
  }

  async componentWillMount() {
    const { address } = this.props.match.params
    await this.props.usbuild.loadUser(address)
  }

  renderTextInput = (index: number) => {
    const url = this.state.urls[index]
    const { address } = this.props.match.params
    const { auth, usbuild } = this.props
    const { urls } = this.state
    const user = usbuild.user(address)
    return (
      <div
        key={index}
        style={{
          display: 'flex',
          backgroundColor: offBlack,
          padding: 8,
          textAlign: 'center',
          alignSelf: 'center',
          alignItems: 'center',
        }}
      >
        <div>
          Sample URL {index + 1}:
        </div>
        <Input
          style={{ margin: 4 }}
          onChange={(e: any) => {
            const newUrls = [...urls]
            newUrls[index] = e.target.value
            this.setState({ urls: newUrls})
          }}
          placeholder={user.sampleUrls && user.sampleUrls.length > index && user.sampleUrls[index] || ''}
          value={url}
        />
        <button onClick={async () => {
          await auth.send({
            to: user.contractAddress,
            data: user.contract.methods.setSampleUrl(url, index).encodeABI()
          })
        }}>Update</button>
      </div>
    )
  }

  renderSkillInput = (index: number) => {
    const { address } = this.props.match.params
    const { auth, usbuild } = this.props
    const { skills } = this.state
    const user = usbuild.user(address)
    return (
      <div
        key={index}
        style={{
          display: 'flex',
          backgroundColor: offBlack,
          padding: 8,
          textAlign: 'center',
          alignSelf: 'center',
          alignItems: 'center',
        }}
      >
        <div>
          Add Skill 1:
        </div>
        <Input
          style={{ margin: 4 }}
          onChange={(e: any) => {
            const newSkills = [...skills]
            newSkills[index] = e.target.value
            this.setState({ skills: newSkills })
          }}
          placeholder={user.skills && user.skills.length > index && user.skills[index] || ''}
          value={skills[index]}
        />
        <button onClick={async () => {
          await auth.send({
            to: user.contractAddress,
            data: user.contract.methods.setSkill(skills[index], index).encodeABI()
          })
        }}>Update</button>
      </div>
    )
  }

  render() {
    const { display, usbuild, auth, match: { params: { address } }} = this.props
    const user = usbuild.user(address)
    const ownProfile = usbuild.user(auth.activeAddress)
    const isOwnProfile = address.toLowerCase() === idx(ownProfile, (_) => _.contractAddress.toLowerCase())
    return (
      <div style={{
        maxWidth: 900,
        minHeight: display.height,
        padding: 8,
        color: white,
        display: 'flex',
        flexDirection: 'column',
      }}>
        <Header title={`${user.name || 'Unnamed User'} ${user.owner && user.owner === auth.activeAddress ? '(You)' : ''}`}/>
        <div style={{ height: 50 }} />
        {isOwnProfile ? (
          <div
            style={{
              display: 'flex',
              backgroundColor: offBlack,
              padding: 8,
              textAlign: 'center',
              alignSelf: 'center',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <div>
              {usbuild.web3.utils.fromWei(user.balance || '0')} Eth
            </div>
            <button onClick={async () => {
            }}>Withdraw</button>
          </div>
        ) : null}
        <div
          style={{
            display: 'flex',
            backgroundColor: offBlack,
            padding: 8,
            textAlign: 'center',
            alignSelf: 'center',
            alignItems: 'center',
          }}
        >
          <div>
            Name:
          </div>
          <Input
            style={{ margin: 4 }}
            onChange={(e: any) => {
              this.setState({ nameText: e.target.value })
            }}
            placeholder={user.name}
            value={this.state.nameText}
          />
          <button onClick={async () => {
            await auth.send({
              to: user.contractAddress,
              data: user.contract.methods.setName(this.state.nameText).encodeABI()
            })
          }}>Update</button>
        </div>
        <div
          style={{
            display: 'flex',
            backgroundColor: offBlack,
            padding: 8,
            textAlign: 'center',
            alignSelf: 'center',
            alignItems: 'center',
          }}
        >
          <div>
            Email:
          </div>
          <Input
            style={{ margin: 4 }}
            onChange={(e: any) => {
              this.setState({ emailText: e.target.value })
            }}
            placeholder={user.email}
            value={this.state.emailText}
          />
          <button onClick={async () => {
            await auth.send({
              to: user.contractAddress,
              data: user.contract.methods.setEmail(this.state.emailText).encodeABI()
            })
          }}>Update</button>
        </div>
        {Array(10).fill(null).map((_, index) => this.renderTextInput(index))}
        {Array(10).fill(null).map((_, index) => this.renderSkillInput(index))}
        <Footer />
      </div>
    )
  }
}

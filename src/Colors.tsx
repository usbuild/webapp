export const white = '#f5ffff'
export const black = '#151515'
export const offBlack = '#383836'
export const goldDark = '#786544'
export const goldLight = '#ead3a1'

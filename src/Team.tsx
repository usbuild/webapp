import React from 'react'
import { inject, observer } from 'mobx-react'
import AuthStore from './stores/auth'
import UsBuildStore, { Team } from './stores/usbuild'
import Header from './components/Header'
import { white, offBlack } from './Colors'
import Input from './components/Input'
import Footer from './components/Footer'
import DisplayStore from './stores/display'
import uuid from 'uuid'
import UserCell from './components/UserCell'
import Link from './components/Link'

@inject('auth', 'usbuild', 'display')
@observer
export default class _Team extends React.Component<{
  match: {
    params: {
      address: string
    }
  }
  auth?: AuthStore
  usbuild?: UsBuildStore
  display?: DisplayStore
}> {
  state = {
    team: {} as Team,
    nameText: '',
    inviteUrl: '',
  }

  async componentWillMount() {
    const { address } = this.props.match.params
    const team = new Team(address, this.props.usbuild.web3)
    this.setState({ team })
  }

  createTeamInvite = async () => {
    const { auth } = this.props
    const { team } = this.state
    try {
      const secret = uuid.v4()
      const hash = team.web3.utils.keccak256(secret)
      await auth.send({
        to: team.contractAddress,
        data: team.contract.methods.addInviteHash(hash).encodeABI(),
      })
      const subscription = team.web3.eth.subscribe('newBlockHeaders', async (err: any) => {
        if (err) return
        subscription.unsubscribe()
        this.setState({
          inviteUrl: `https://website.usbuild.now.sh/team/invite/${team.contractAddress}/${secret}`,
        })
      }).on('data', () => {})
    } catch (err) {
      console.log('Invite generation error', err)
    }
  }

  render() {
    const { auth, usbuild, display } = this.props
    const { team } = this.state
    return (
      <div style={{
        minHeight: display.height,
        maxWidth: 900,
        margin: 'auto',
        padding: 8,
        color: white,
        display: 'flex',
        flexDirection: 'column',
      }}>
        <Header title={`${team.name || 'Unnamed Team'}`} />
        <div style={{ height: 50 }} />
        <div
          style={{
            display: 'flex',
            backgroundColor: offBlack,
            padding: 8,
            textAlign: 'center',
            alignSelf: 'center',
            alignItems: 'center',
            flexDirection: 'column',
          }}
        >
          {usbuild.web3.utils.fromWei(team.balance)} Eth
          <button onClick={() => {}}>
            Settle
          </button>
        </div>
        <div
          style={{
            display: 'flex',
            backgroundColor: offBlack,
            padding: 8,
            textAlign: 'center',
            alignSelf: 'center',
            alignItems: 'center',
          }}
        >
          <div>
            Name:
          </div>
          <Input
            style={{ margin: 4 }}
            onChange={(e: any) => {
              this.setState({ nameText: e.target.value })
            }}
            placeholder={team.name}
            value={this.state.nameText}
          />
          <button onClick={async () => {
            await auth.send({
              to: team.contractAddress,
              data: team.contract.methods.setName(this.state.nameText).encodeABI()
            })
          }}>Update</button>
        </div>
        <div
          style={{
            display: 'flex',
            backgroundColor: offBlack,
            padding: 8,
            textAlign: 'center',
            alignSelf: 'center',
            alignItems: 'center',
            flexDirection: 'column',
          }}
        >
          <Input
            style={{ margin: 4 }}
            value={this.state.inviteUrl}
            readOnly
          />
          <button onClick={this.createTeamInvite}>
            Generate Invite URL
          </button>
        </div>
        <div style={{ height: 20 }} />
        <div style={{ margin: 4, textAlign: 'center', fontSize: 18 }}>
          Members
        </div>
        <div style={{
          display: 'flex',
          flexDirection: 'column',
        }}>
          {team.users.map((user, index) => (
            <div
              key={index}
              style={{
                display: 'flex',
                backgroundColor: offBlack,
                padding: 8,
                margin: 4,
                textAlign: 'center',
                alignSelf: 'center',
                alignItems: 'center',
                flexDirection: 'column',
              }}
            >
              <Link to={`/user/${user.contractAddress}`}>
                {user.name}
              </Link>
            </div>
          ))}
        </div>
        <Footer />
      </div>
    )
  }
}

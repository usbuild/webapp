import React from 'react'
import { white, black, offBlack } from './Colors'
import Header from './components/Header'
import { inject, observer } from 'mobx-react'
import AuthStore from './stores/auth'
import UsBuildStore, { Team } from './stores/usbuild'
import TeamCell from './components/TeamCell'
import Popup from './components/Popup'
import CreateTeam from './components/CreateTeam'
import Footer from './components/Footer'
import DisplayStore from './stores/display'

@inject('auth', 'usbuild', 'display')
@observer
export default class Home extends React.Component<{
  auth?: AuthStore
  usbuild?: UsBuildStore
  display?: DisplayStore
}> {
  state = {
    inviteUrl: '',
    teamCreateVisible: false,
  }

  render() {
    const { auth, usbuild, display } = this.props
    return (
      <>
        <Popup
          onClick={() =>
            this.setState({
              teamCreateVisible: false,
            })}
          visible={this.state.teamCreateVisible}
        >
          <CreateTeam onCompleted={() => this.setState({ teamCreateVisible: false })} />
        </Popup>
        <div style={{
          minHeight: display.height,
          maxWidth: 900,
          padding: 8,
          color: white,
          display: 'flex',
          flexDirection: 'column',
        }}>
          <Header />
          <div style={{ height: 50 }} />
          {usbuild.teams.map((team, index) => <TeamCell key={index} team={team} />)}
          <div
            style={{
              display: 'flex',
              backgroundColor: offBlack,
              padding: 8,
              maxWidth: 100,
              textAlign: 'center',
              cursor: 'pointer',
              alignSelf: 'center',
              margin: 8,
            }}
            onClick={() => {
              this.setState({
                teamCreateVisible: true,
              })
            }}
          >
            Create Team
          </div>
          <Footer />
        </div>
      </>
    )
  }
}

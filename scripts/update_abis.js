#!/usr/bin/env node

const fs = require('fs')
const path = require('path')

const abis = path.join(__dirname, '../../contracts/build/contracts/')
const usbuild = require(path.join(abis, 'UsBuild.json')).abi
const user = require(path.join(abis, 'User.json')).abi
const team = require(path.join(abis, 'Team.json')).abi

const stores = path.join(__dirname, '../src/stores')
fs.writeFileSync(path.join(stores, 'usbuildABI.ts'), `export default ${JSON.stringify(usbuild)}`)
fs.writeFileSync(path.join(stores, 'userABI.ts'), `export default ${JSON.stringify(user)}`)
fs.writeFileSync(path.join(stores, 'teamABI.ts'), `export default ${JSON.stringify(team)}`)

console.log('updated abis')

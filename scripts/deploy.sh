#!/bin/sh

set -e

npm run build
now --target production

# If .env doesn't exist bail
if [ -e .env ]; then
  source .env
else
  echo 'Skipping discord notification'
  exit 0
fi

# If discord webhook url is empty exit
if [ -z "$DISCORD_WEBHOOK_URL" ]; then
  echo 'Skipping discord notification, wehook not set'
  exit 0
else
  curl -X POST --data '{"content": "New webapp deployed at: https://website.usbuild.now.sh"}' -H 'Content-Type: application/json' $DISCORD_WEBHOOK_URL
fi
